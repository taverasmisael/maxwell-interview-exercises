export interface Discount {
  quantity: number
  value: number
}
