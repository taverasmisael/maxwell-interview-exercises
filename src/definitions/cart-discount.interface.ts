export interface CartDiscount {
  productId: string
  productName: string
  price: number
}
