import { useMemo } from 'react'
import { Discount } from '../../definitions/discount.interface'
import { formatProductPrice } from '../../helpers/products'
import styles from './Product.module.css'

function ProductPrice({ price, discount }: ProductPriceProps) {
  const strPrice = useMemo(() => formatProductPrice(price), [price])
  return (
    <p className="mb-3">
      {discount ? (
        <>
          <span className={styles.newPice}>
            {discount.quantity} x {formatProductPrice(discount.value)}
          </span>
          <br />
          <small>
            <span className={styles.oldPrice}>or {strPrice} each </span>
          </small>
        </>
      ) : (
        strPrice
      )}
    </p>
  )
}

interface ProductPriceProps {
  price: number
  discount?: Discount
}

export default ProductPrice
