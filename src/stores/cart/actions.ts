import {
  CartAction,
  CartActionType,
} from '../../definitions/cart-action.interface'
import { CartItem } from '../../definitions/cart-item.interface'
import { Product } from '../../definitions/product.interface'
import { createCartItem } from '../../helpers/cart'

export const addToCart = (
  product: Product,
  quantity: number
): CartAction<CartItem> => ({
  type: CartActionType.AddItem,
  payload: createCartItem(product, quantity),
})

export const removeFromCart = (productId: string): CartAction<string> => ({
  type: CartActionType.RemoveItem,
  payload: productId,
})

export const clearCart = (): CartAction<void> => ({
  type: CartActionType.Clear,
})
