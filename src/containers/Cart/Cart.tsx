import classNames from 'classnames'

import CartEntry from '../../components/CartEntry'
import EmptyCart from '../../components/EmptyCart'
import { ReactComponent as CloseIcon } from '../../assets/close.svg'
import { useShowCart } from '../../stores/show-cart/hooks'

import styles from './Cart.module.css'
import {
  useCartDetails,
  useClearCart,
  useRemoveFromCart,
} from '../../stores/cart/hooks'
import { formatProductPrice } from '../../helpers/products'

function Cart() {
  const [isOpen, { hideCart }] = useShowCart()
  const { items, discounts, subTotal, total } = useCartDetails()
  const pay = useClearCart()
  const removeItem = useRemoveFromCart()

  return (
    <>
      <div
        className={classNames(styles.backdrop, {
          [styles.backdrop__isOpen]: isOpen,
        })}
        onClick={hideCart}
      />
      <aside
        className={classNames(styles.container, {
          [styles.container__isOpen]: isOpen,
        })}
      >
        <h3 className={styles.title}>Your cart</h3>
        <button
          type="button"
          className="absolute top-5 right-6 bg-gray-200 text-gray-800 rounded-full h-8 w-8 p-2"
          onClick={hideCart}
        >
          <span className="sr-only">Close Cart</span>
          <CloseIcon className="fill-current h-4 w-4" />
        </button>

        {items.length ? (
          <>
            <ul className={styles.items}>
              {items.map((item) => (
                <li key={item.id} className={styles.item}>
                  <CartEntry
                    name={item.name}
                    onRemove={() => removeItem(item.id)}
                    price={item.price}
                    quantity={item.quantity}
                  />
                </li>
              ))}
            </ul>
            {!!discounts.length && (
              <div className={styles.discounts}>
                <div className={styles.subTotal}>
                  <span>Sub total</span>
                  <span>{formatProductPrice(subTotal)}</span>
                </div>
                <p>Discounts</p>
                <ul>
                  {discounts.map((discount) => (
                    <li key={discount.productId} className={styles.item}>
                      <CartEntry
                        isDiscount
                        quantity={1}
                        name={discount.productName}
                        price={discount.price}
                      />
                    </li>
                  ))}
                </ul>
              </div>
            )}
            <div className={styles.total}>
              <span>Total:</span>
              <span>{formatProductPrice(total)}</span>
            </div>

            <button className={styles.cta} onClick={pay}>
              PAY! (it's free)
            </button>
          </>
        ) : (
          <EmptyCart />
        )}
      </aside>
    </>
  )
}

export default Cart
