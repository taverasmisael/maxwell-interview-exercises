export enum RequestStatus {
  idle,
  loading,
  success,
  error,
}
