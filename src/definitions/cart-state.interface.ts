import { CartItem } from './cart-item.interface'

export interface CartState {
  items: CartItem[]
}
