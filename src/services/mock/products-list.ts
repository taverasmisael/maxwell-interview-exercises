import { nanoid } from 'nanoid'
import { Product } from '../../definitions/product.interface'

const productsList: Product[] = [
  {
    id: nanoid(),
    name: 'Milk',
    price: 3.97,
    discount: { quantity: 2, value: 5.0 },
  },
  {
    id: nanoid(),
    name: 'Bread',
    price: 2.17,
    discount: { quantity: 3, value: 6.0 },
  },
  { id: nanoid(), name: 'Banana', price: 0.99 },
  { id: nanoid(), name: 'Apple', price: 0.89 },
]

export default productsList
