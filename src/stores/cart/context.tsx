import { useReducer, createContext, Dispatch } from 'react'
import { CartAction } from '../../definitions/cart-action.interface'

import { cartReducer } from './reducer'
import { initialState } from './consts'

export const CartStateContext = createContext(initialState)
export const CartDispatchContext = createContext({} as Dispatch<CartAction>)

export function CartProvider({ children }: { children: React.ReactNode }) {
  const [state, dispatch] = useReducer(cartReducer, initialState)
  return (
    <CartStateContext.Provider value={state}>
      <CartDispatchContext.Provider value={dispatch}>
        {children}
      </CartDispatchContext.Provider>
    </CartStateContext.Provider>
  )
}
