import Product from '../../components/Product'
import { Product as ProductInterface } from '../../definitions/product.interface'
import { RequestStatus } from '../../definitions/request-status.enum'
import { useProducts } from '../../hooks/useProducts'
import { useAddToCart } from '../../stores/cart/hooks'

function ProductsList() {
  const { products, requestStatus } = useProducts()

  const addProduct = useAddToCart()

  const handleAddProduct = (product: ProductInterface) => (quantity: number) =>
    addProduct(product, quantity)

  return (
    <main>
      <h2 className="text-4xl mb-4">Products</h2>
      {requestStatus === RequestStatus.loading && (
        <p className="text-xl font-bold my-4">Loading...</p>
      )}
      {requestStatus === RequestStatus.error && <p>Error!</p>}
      {requestStatus === RequestStatus.success && (
        <div className="grid sm:grid-cols-3 lg:grid-cols-4 gap-4">
          {products.map((product) => (
            <Product
              key={product.id}
              id={product.id}
              name={product.name}
              price={product.price}
              discount={product.discount}
              onAdd={handleAddProduct(product)}
            />
          ))}
        </div>
      )}
    </main>
  )
}

export default ProductsList
