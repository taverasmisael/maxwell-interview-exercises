import { Product } from '../definitions/product.interface'

const priceFormater = Intl.NumberFormat('en', {
  currency: 'USD',
  style: 'currency',
})

export function calculateTotal(product: Product, quantity = 1): number {
  const basePrice = product.price
  if (product.discount) {
    const mod = quantity % product.discount.quantity
    const regularPrice = basePrice * mod
    const inOffer = Math.floor(quantity / product.discount.quantity)
    const offer = inOffer * product.discount.value
    return Number((offer + regularPrice).toFixed(2))
  }

  return basePrice
}

export function formatProductPrice(price: number): string {
  return priceFormater.format(price)
}
