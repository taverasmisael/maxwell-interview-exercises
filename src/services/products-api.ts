import { sleep } from '../helpers/utils'
import productsList from './mock/products-list'

export async function getProducts() {
  await sleep(1000)
  return productsList
}
