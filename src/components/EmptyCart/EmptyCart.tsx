import emptyCart from '../../assets/undraw_empty_cart.png'

function EmptyCart() {
  return (
    <div className="flex flex-col items-center justify-center -mt-10">
      <img src={emptyCart} alt="" className="mb-2 object-contain h-60 w-full" />
      <h4 className="text-center text-xl">Your cart is empty</h4>
      <p className="text-gray-400 text-center text-base">
        Add products from the left side and watch the magic{' '}
        <span aria-hidden>✨</span>!
      </p>
    </div>
  )
}

export default EmptyCart
