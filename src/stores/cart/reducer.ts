import {
  CartAction,
  CartActionType,
} from '../../definitions/cart-action.interface'
import { CartState } from '../../definitions/cart-state.interface'
import { initialState } from './consts'

export function cartReducer(
  state = initialState,
  action: CartAction
): CartState {
  switch (action.type) {
    case CartActionType.AddItem:
      return {
        ...state,
        items: [...state.items, action.payload],
      }
    case CartActionType.RemoveItem:
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.payload),
      }

    case CartActionType.Clear:
      return {
        ...state,
        items: [],
      }

    default:
      return state
  }
}
