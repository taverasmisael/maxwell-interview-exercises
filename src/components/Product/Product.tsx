import { Discount } from '../../definitions/discount.interface'
import AddProduct from '../AddProduct'

import ProductPrice from './ProductPrice'
import styles from './Product.module.css'

function Product({ name, price, discount, id, onAdd }: ProductProps) {
  return (
    <div className={styles.product}>
      <h3 className={styles.name}>{name}</h3>

      <ProductPrice price={price} discount={discount} />

      <AddProduct name={name} onSubmit={onAdd} />
    </div>
  )
}

interface ProductProps {
  name: string
  price: number
  discount?: Discount
  id: string
  onAdd: (quantity: number) => void
}

export default Product
