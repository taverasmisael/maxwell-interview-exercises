import { useContext, useMemo } from 'react'
import { Product } from '../../definitions/product.interface'
import { addToCart, clearCart, removeFromCart } from './actions'
import { CartStateContext, CartDispatchContext } from './context'
import {
  getCartDiscounts,
  getCartItems,
  getCartSubTotal,
  getCartTotal,
} from './selectors'

export function useCartState() {
  const cart = useContext(CartStateContext)
  return cart
}

export function useCartTotalItems() {
  const cart = useContext(CartStateContext)
  return cart.items.length
}

export function useCartItems() {
  const cart = useCartState()
  return getCartItems(cart)
}

export function useCartSubTotal() {
  const cart = useCartState()
  return getCartSubTotal(cart)
}

export function useCartDiscounts() {
  const cart = useCartState()
  return getCartDiscounts(cart)
}

export function useCartTotal() {
  const cart = useCartState()
  return getCartTotal(cart)
}

export function useCartDetails() {
  const cart = useCartState()

  const state = useMemo(
    () => ({
      items: getCartItems(cart),
      subTotal: getCartSubTotal(cart),
      discounts: getCartDiscounts(cart),
      total: getCartTotal(cart),
    }),
    [cart]
  )

  return state
}

// Actions

export function useCartDispatch() {
  const dispatch = useContext(CartDispatchContext)
  return dispatch
}

export function useAddToCart() {
  const dispatch = useCartDispatch()
  return (product: Product, quantity = 1) =>
    dispatch(addToCart(product, quantity))
}

export function useRemoveFromCart() {
  const dispatch = useCartDispatch()
  return (id: string) => dispatch(removeFromCart(id))
}

export function useClearCart() {
  const dispatch = useCartDispatch()
  return () => dispatch(clearCart())
}
