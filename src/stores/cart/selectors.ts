import { createSelector } from 'reselect'
import { CartState } from '../../definitions/cart-state.interface'
import { sumCart, calculateDiscounts } from '../../helpers/cart'

export const getCartItems = (state: CartState) => state.items

export const getCartSubTotal = createSelector(getCartItems, sumCart)

export const getCartDiscounts = createSelector(getCartItems, calculateDiscounts)

export const getCartTotal = createSelector(
  getCartSubTotal,
  getCartDiscounts,
  (subTotal, discounts) => subTotal - sumCart(discounts)
)
