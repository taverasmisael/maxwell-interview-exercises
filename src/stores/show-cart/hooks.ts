import { useContext, useMemo } from 'react'
import {
  ShowCartDispatchContext,
  ShowCartStateContext,
  ShowCartState,
  ShowCartDispatch,
} from './context'

export const useShowCart = () => {
  const isVisible = useContext(ShowCartStateContext)
  const dispatchers = useContext(ShowCartDispatchContext)

  const showCart: [ShowCartState, ShowCartDispatch] = useMemo(
    () => [isVisible, dispatchers],
    [isVisible, dispatchers]
  )

  return showCart
}
