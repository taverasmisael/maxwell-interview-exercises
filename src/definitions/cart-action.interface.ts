export interface CartAction<T = any> {
  type: CartActionType
  payload?: T
}

export enum CartActionType {
  AddItem = 'ADD_ITEM',
  RemoveItem = 'REMOVE_ITEM',
  Clear = 'CLEAR_CART',
}
