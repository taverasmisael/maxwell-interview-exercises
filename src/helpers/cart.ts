import { nanoid } from 'nanoid'
import { CartDiscount } from '../definitions/cart-discount.interface'
import { CartItem } from '../definitions/cart-item.interface'
import { Product } from '../definitions/product.interface'
import { calculateTotal } from './products'

export function calculateCartTotal(items: CartItem[]): number {
  const subTotal = sumCart(items)
  const discounts = sumCart(calculateDiscounts(items))

  return subTotal - discounts
}

export const createCartItem = (
  product: Product,
  quantity: number
): CartItem => {
  const { id, name, price } = product
  return {
    id: nanoid(),
    productId: id,
    name,
    price,
    quantity,
    discount: product.discount,
  }
}

export function calculateDiscounts(items: CartItem[]): CartDiscount[] {
  const grouppedItems = items.reduce((p, c) => {
    if (c.discount) {
      const allItems = new Array(c.quantity || 1).fill(c)
      return {
        ...p,
        [c.productId]: [...(p[c.productId] || []), ...allItems],
      }
    }

    return p
  }, {} as { [k: string]: CartItem[] })

  return Object.keys(grouppedItems).reduce((prev, productId) => {
    const items = grouppedItems[productId]
    const product = items[0]

    // This will NEVER be zero, because we already checked for discount
    if (items.length >= (product.discount?.quantity || 0)) {
      const subTotal = items.length * product.price
      const total = calculateTotal(product, items.length)

      return [
        ...prev,
        {
          productId,
          productName: product.name,
          price: Number((subTotal - total).toFixed(2)),
        },
      ]
    }

    return prev
  }, [] as CartDiscount[])
}

export function sumCart(items: { price: number; quantity?: number }[]): number {
  return items.reduce((p, c) => p + c.price * (c.quantity || 1), 0)
}
