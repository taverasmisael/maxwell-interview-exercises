import classNames from 'classnames'
import { formatProductPrice } from '../../helpers/products'
import { ReactComponent as RemoveIcon } from '../../assets/remove.svg'

import styles from './CartEntry.module.css'

function CartEntry({
  name,
  price,
  quantity,
  onRemove,
  isDiscount,
}: CartEntryProps) {
  return (
    <span
      className={classNames(styles.container, {
        [styles.isDiscount]: isDiscount,
      })}
    >
      {!isDiscount && <span className={styles.quantity}>{quantity}</span>}
      <strong className={styles.name}>{name}</strong>
      <span
        className={classNames(styles.price, {
          [styles.discountPrice]: isDiscount,
        })}
      >{`${isDiscount ? '- ' : ''}${formatProductPrice(price)}`}</span>
      {!!onRemove && (
        <button className={styles.button} onClick={onRemove}>
          <span className="sr-only">Remove items</span>
          <RemoveIcon className={styles.icon} />
        </button>
      )}
    </span>
  )
}

interface CartEntryProps {
  name: string
  price: number
  quantity: number
  isDiscount?: boolean
  onRemove?: () => void
}

export default CartEntry
