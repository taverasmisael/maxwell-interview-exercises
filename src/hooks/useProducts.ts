import { useState, useEffect } from 'react'
import { Product } from '../definitions/product.interface'
import { RequestStatus } from '../definitions/request-status.enum'

import { getProducts } from '../services/products-api'

export function useProducts() {
  const [products, setProducts] = useState<Product[]>([])
  const [requestStatus, setRequestStatus] = useState<RequestStatus>(
    RequestStatus.idle
  )

  useEffect(() => {
    async function fetchProducts() {
      setRequestStatus(RequestStatus.loading)
      try {
        const allProducts = await getProducts()
        setProducts(allProducts)
        setRequestStatus(RequestStatus.success)
      } catch (error) {
        setProducts([])
        setRequestStatus(RequestStatus.error)
      }
    }

    fetchProducts()
  }, [])

  // No need to manage error states or anything like that.
  return {
    products,
    requestStatus,
  }
}
