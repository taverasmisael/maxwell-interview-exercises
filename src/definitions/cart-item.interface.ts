import { Discount } from './discount.interface'

export interface CartItem {
  id: string
  productId: string
  name: string
  price: number
  quantity: number
  discount?: Discount
}
