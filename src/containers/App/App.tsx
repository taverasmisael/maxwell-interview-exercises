import Header from '../../components/Header'
import Cart from '../Cart'
import ProductsList from '../ProductsList'

import { ShowCartContext } from '../../stores/show-cart/context'
import { CartProvider } from '../../stores/cart/context'

function App() {
  return (
    <CartProvider>
      <ShowCartContext>
        <Header />
        <Cart />
      </ShowCartContext>
      <div className="container mx-auto p-4">
        <ProductsList />
      </div>
    </CartProvider>
  )
}

export default App
