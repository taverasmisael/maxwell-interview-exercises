import { CartState } from '../../definitions/cart-state.interface'

export const initialState: CartState = {
  items: [],
}
