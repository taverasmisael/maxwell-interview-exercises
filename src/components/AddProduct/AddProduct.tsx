import { FormEvent, useState, ChangeEvent } from 'react'
import classNames from 'classnames'
import { ReactComponent as AddToCartIcon } from '../../assets/add_to_cart.svg'

import styles from './AddProduct.module.css'

function AddProduct({ onSubmit, name }: AddProductProps) {
  const [value, setValue] = useState(1)

  const updateAmount = ({ target }: ChangeEvent<HTMLInputElement>) => {
    if (!target.value) {
      setValue(0)
    }
    const value = parseInt(target.value, 10)
    if (!Number.isNaN(value)) {
      if (value <= 999) setValue(value)
    }
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    onSubmit(value)
    setValue(1)
  }

  return (
    <form onSubmit={handleSubmit} className={styles.form}>
      <label className={styles.label}>
        <span className="sr-only">Quantity of {name} to add</span>
        <input
          type="text"
          name="quantity"
          placeholder="1"
          className={styles.input}
          value={value}
          onChange={updateAmount}
        />
      </label>
      <button
        title="Add to cart"
        type="submit"
        className={classNames(styles.button, {
          [styles.buttonDisabled]: value === 0,
        })}
        disabled={value === 0}
      >
        <AddToCartIcon className={styles.icon} />
        <span className="hidden xs:inline-block">Add to cart</span>
      </button>
    </form>
  )
}

interface AddProductProps {
  onSubmit: (quantity: number) => void
  name: string
}

export default AddProduct
