import { useMemo, FC, createContext, useState } from 'react'

export const ShowCartStateContext = createContext(false)
export const ShowCartDispatchContext = createContext({
  showCart(): void {
    throw new Error('Not implemented')
  },
  hideCart(): void {
    throw new Error('Not implemented')
  },
})

export const ShowCartContext: FC<any> = ({ children }) => {
  const [isVisible, setIsVisible] = useState(false)

  const showCart = () => setIsVisible(true)
  const hideCart = () => setIsVisible(false)

  const dispatchers = useMemo(() => ({ showCart, hideCart }), [])

  return (
    <ShowCartStateContext.Provider value={isVisible}>
      <ShowCartDispatchContext.Provider value={dispatchers}>
        {children}
      </ShowCartDispatchContext.Provider>
    </ShowCartStateContext.Provider>
  )
}

export type ShowCartState = boolean
export type ShowCartDispatch = {
  showCart(): void
  hideCart(): void
}
