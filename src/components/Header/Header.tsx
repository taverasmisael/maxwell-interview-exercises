import { useContext } from 'react'
import { ReactComponent as Logo } from '../../assets/logo.svg'
import { ReactComponent as CartIcon } from '../../assets/cart.svg'
import { ShowCartDispatchContext } from '../../stores/show-cart/context'
import { useCartTotalItems } from '../../stores/cart/hooks'

import styles from './Header.module.css'

function Header() {
  const { showCart } = useContext(ShowCartDispatchContext)
  const items = useCartTotalItems()

  return (
    <header className={styles.header}>
      <div className={styles.center}>
        <div className={styles.content}>
          <div className="flex items-center">
            <Logo className={styles.logo} title="MiniMarket - React" />
            <h1 className={styles.brand}>MiniMarket - React</h1>
          </div>
          <div className="flex items-center">
            <button className={styles.cartButton} onClick={showCart}>
              <CartIcon className="h-6 w-6 fill-current z-10" />
              {items > 0 && <span className={styles.cartBadge}>{items}</span>}
            </button>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
