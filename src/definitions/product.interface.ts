import { Discount } from './discount.interface'

export interface Product {
  id: string
  price: number
  name: string
  discount?: Discount
}
